const router = require('express').Router()
const multer  = require('multer')
var path = require('path');
console.log(path.join(__dirname,'/tmp/my-uploads'));
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname,'/tmp/my-uploads'))
    },
    filename: function (req, file, cb) {
     
      cb(null, "dataFile.xlsx")
    }
  })
  
  const upload = multer({ storage: storage })


const {
    getDetails,
    uploadData
} = require('./controller')
router.get("/helathcheck", (req,res)=>{res.status(200).send("Server is up and running")})
router.get('/details/:limit', getDetails);
router.post('/upload', upload.single('myFile'), uploadData);


module.exports = router