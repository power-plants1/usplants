const {getDetailsService, uploadDataService} = require("./service");

const getDetails = async(req, res)=>{
    try{
        const data = await getDetailsService(req.params, req.query);
        res.status(200).json({data})

    }catch(error){
        console.log("error in get details ", error);
    }
}
const uploadData = async(req, res)=>{
    try{
        await uploadDataService(req.file);
        res.status(201).json({"message": "Data uploaded successfully"});
    }catch(error){
        console.log("error in upload data controller ", error);
    }
}
module.exports = {getDetails,
                uploadData}