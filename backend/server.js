const http = require("http");
const express = require("express");


const cors = require('cors');
const bodyParser = require('body-parser');

const routes = require("./routes");

const mongoose = require("mongoose");
const MongoClient = require('mongodb').MongoClient;

process.on("uncaughtException", (err) => {
  console.log("UNCAUGHT EXCEPTION, APP SHUTTING NOW!!");
  console.log(err.message, err.name);
  process.exit(1);
});

const app = express();
  const server = http.createServer(app);
  

  app.use(cors());
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

const url = "mongodb+srv://vishal:gAgQIVTrncRD288L@cluster0.vfyry.mongodb.net?retryWrites=true&w=majority";
// const client = new MongoClient(url);

async function init(){

  const mongoclintInstance = await MongoClient.connect(url, {useNewUrlParser: true});
  console.log("DB connected successfully");
  global.PlantData = mongoclintInstance.db("usplants").collection("plant_data");
  global.allUsData = mongoclintInstance.db("usplants").collection("all_us_data");

  let port = process.env.PORT || 80
  app.use("/usplants", routes);
  app.use("", (req,res)=>{res.status(200).send("Server is up and running")})
  server.listen(port, ()=>{
    console.log(`server running at port ${port}`)
  })

}
init();







