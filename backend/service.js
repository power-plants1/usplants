const fs = require("fs");
const reader = require('xlsx');

const getDetailsService = async (limit, filter)=>{
    try{
        let findFor = {}
        if(filter && filter.hasOwnProperty("stateCode")){
            findFor = {
                "plantStateAbbreviation": filter.stateCode
            }
        }
        let getPlanDetails = await global.PlantData.find(findFor).sort({"annualNetGenerationMWh": -1}).limit(parseInt(limit.limit)).toArray(); 
        let totalUSGeneration = await global.allUsData.findOne();
        
        console.log("inside get details service ");
        return {getPlanDetails, totalUSGeneration};

    }catch(error){
        console.log("error in get detatils service ", error);
    }
}
const uploadDataService = async(file)=>{
    try{
        await global.PlantData.deleteMany({});
    
        console.log(file)
        let fileData = reader.readFile(file.path);
        let spreadSheets = []
        const sheets = fileData.SheetNames
        for(let i = 0; i < sheets.length; i++)
        {
            const temp = reader.utils.sheet_to_json(fileData.Sheets[fileData.SheetNames[i]])
            spreadSheets.push({"sheetName": fileData.SheetNames[i], "data": temp})
        }
        let plantDetails = spreadSheets[3].data;  
        let sheetNameToSave = spreadSheets[3].sheetName;
        let stateWiseData = spreadSheets[4].data;
        savePlantDetails(plantDetails, sheetNameToSave, stateWiseData);

    }catch(error){
        console.log("error in upload data service ", error);
    }
}

const savePlantDetails = async(plantDetails, sheetNameToSave, stateWiseData)=>{
    try{
        await Promise.all(plantDetails.map(async (details, index)=>{
            console.log("index is ", index);
            let plantEssentials = {
                "plantStateAbbreviation": details['Plant state abbreviation'],
                'plantName': details['Plant name'],
                "lat": details['Plant latitude'],
                "lon": details['Plant longitude'],
                "annualNetGenerationMWh": details['Plant annual net generation (MWh)'],
            }
            stateWiseData.map(stateData=>{
                if(details['Plant state abbreviation'] == stateData['State abbreviation']){
                    let platnAnnualNetGen = details['Plant annual net generation (MWh)'];
                    let stateAnnualNetGen = stateData['State annual net generation (MWh)'];
                    plantEssentials['stateAnnualNetGenerationMWh'] = stateData['State annual net generation (MWh)'];
                    plantEssentials['percentageGeneration'] =  (platnAnnualNetGen / stateAnnualNetGen) * 100;
                }
            })
           console.log(plantEssentials);
           let respo =  await global.PlantData.insertOne(plantEssentials)   
           console.log(respo)
        }))
        console.log("Data insertion complete");
    }catch(error){
        console.log("error in save plant details ",error);
    }
}
// 53,792,187


module.exports = {getDetailsService, uploadDataService}