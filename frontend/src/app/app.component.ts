import { AfterViewInit, Component } from '@angular/core';
// import * as Highcharts from 'highcharts/highmaps';
// import HighchartsMore from 'highcharts/highcharts-more';
// import HighchartsSolidGauge from 'highcharts/modules/solid-gauge';
// import { ApiServiceService } from './api-service.service';
// import Drilldown from 'highcharts/modules/drilldown';
// Drilldown(Highcharts);

// HighchartsMore(Highcharts);
// HighchartsSolidGauge(Highcharts);

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = 'usplants';

  

  public ngAfterViewInit(): void {
    this.createMapChart();
  }
 

  private createMapChart(): void {
    
    
    // this.service.getTopology().subscribe(data=>{
    //   console.log(data);
    //   let topology = data; 
    //   this.service.getData().subscribe(data=>{
    //     console.log(data);
    //     let pointData:any = data; 
    //     pointData.forEach(function (p:any) {
    //       p.code = p.code.toUpperCase();
    //   });
    //   const chart = Highcharts.mapChart('mapChart', {

    //     chart: {
    //         map: topology,
    //         borderWidth: 1
    //     },
  
    //     title: {
    //         text: 'US population density (/km²)'
    //     },
  
    //     exporting: {
    //         sourceWidth: 600,
    //         sourceHeight: 500
    //     },
  
    //     legend: {
    //         layout: 'horizontal',
    //         borderWidth: 0,
    //         backgroundColor: 'rgba(255,255,255,0.85)',
    //         floating: true,
    //         verticalAlign: 'top',
    //         y: 25
    //     },
  
    //     mapNavigation: {
    //         enabled: true
    //     },
  
    //     colorAxis: {
    //         min: 1,
    //         type: 'logarithmic',
    //         minColor: '#EEEEFF',
    //         maxColor: '#000022',
    //         stops: [
    //             [0, '#EFEFFF'],
    //             [0.67, '#4444FF'],
    //             [1, '#000022']
    //         ]
    //     },
  
    //     series: [{
    //         accessibility: {
    //             point: {
    //                 valueDescriptionFormat: '{xDescription}, {point.value} people per square kilometer.'
    //             }
    //         },
    //         animation: {
    //             duration: 1000
    //         },
    //         data: pointData,
    //         joinBy: ['postal-code', 'code'],
    //         dataLabels: {
    //             enabled: true,
    //             color: '#FFFFFF',
    //             format: '{point.code}'
    //         },
    //         name: 'Population density',
    //         tooltip: {
    //             pointFormat: '{point.code}: {point.value}/km²'
    //         }
    //     }]
    // }as any);
          
    
    // // const chart = Highcharts.chart('chart-gauge', {
    // //   chart: {
    // //     type: 'solidgauge',
    // //   },
    // //   title: {
    // //     text: 'Gauge Chart',
    // //   },
    // //   credits: {
    // //     enabled: false,
    // //   },
    // //   pane: {
    // //     startAngle: -90,
    // //     endAngle: 90,
    // //     center: ['50%', '85%'],
    // //     size: '160%',
    // //     background: {
    // //         innerRadius: '60%',
    // //         outerRadius: '100%',
    // //         shape: 'arc',
    // //     },
    // //   },
    // //   yAxis: {
    // //     min: 0,
    // //     max: 100,
    // //     stops: [
    // //       [0.1, '#55BF3B'], // green
    // //       [0.5, '#DDDF0D'], // yellow
    // //       [0.9, '#DF5353'], // red
    // //     ],
    // //     minorTickInterval: null,
    // //     tickAmount: 2,
    // //     labels: {
    // //       y: 16,
    // //     },
    // //   },
    // //   plotOptions: {
    // //     solidgauge: {
    // //       dataLabels: {
    // //         y: -25,
    // //         borderWidth: 0,
    // //         useHTML: true,
    // //       },
    // //     },
    // //   },
    // //   tooltip: {
    // //     enabled: false,
    // //   },
    // //   series: [{
    // //     name: null,
    // //     data: [this.getRandomNumber(0, 100)],
    // //     dataLabels: {
    // //       format: '<div style="text-align: center"><span style="font-size: 1.25rem">{y}</span></div>',
    // //     },
    // //   }],
    // // } as any);

    //   })

    // })


    


    // setInterval(() => {
    //   chart.series[0].points[0].update(this.getRandomNumber(0, 100));
    // }, 1000);
  }

  

}