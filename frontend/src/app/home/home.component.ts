import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as Highcharts from 'highcharts/highmaps';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsSolidGauge from 'highcharts/modules/solid-gauge';
import { ApiServiceService } from './../api-service.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// import Drilldown from 'highcharts/modules/drilldown';
// Drilldown(Highcharts);

HighchartsMore(Highcharts);
HighchartsSolidGauge(Highcharts);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit, OnInit {
  title = 'usplants';
  topNFormControl = new FormControl('1000', [Validators.required, Validators.pattern("^[0-9]*$")]);
  constructor(private service: ApiServiceService) { }
  topNumber = 1000;
  stateCodeFilter = '';  
  spinner = true;

  myControl = new FormControl('');
  options: string[] = [];
  filteredOptions!: Observable<string[]>;

  public ngAfterViewInit(): void {
    this.createMapChart();
  }
  public ngOnInit(): void {
    // this.createMapChart();

    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || '')),
    );

  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  findBtn(){
    this.topNumber = this.topNFormControl.value;
    console.log(this.topNumber);
    if(this.myControl.value){
      this.stateCodeFilter = `?stateCode=${this.myControl.value.toUpperCase()}`;
    }
    console.log(this.stateCodeFilter);
    this.createMapChart();
    this.options = [];

  }

  private createMapChart(): void {
    
    
    this.service.getTopology().subscribe(data=>{
      console.log(data);
      let topology = data; 
      this.service.getData().subscribe(data=>{
        
        let pointData:any = data; 
        pointData.forEach( (p:any)=>{
          p.code = p.code.toUpperCase();
          this.options.push(p.code);
      });
      console.log(this.options);
      this.service.getPlantData(this.topNumber, this.stateCodeFilter).subscribe((plantData:any)=>{
        
      
    
    console.log("pointData--", pointData);
    console.log(plantData.data)
    let toShow = plantData.data.getPlanDetails;
    let totalUSGeneration = plantData.data.totalUSGeneration.totalInUs;
    
    if(this.stateCodeFilter.length==0){
      toShow.map((data: any)=> {
        data.percentageGeneration = (data.annualNetGenerationMWh / totalUSGeneration )* 100
      })      
    }
    console.log("--to show --",toShow);
    this.spinner = false;
    this.stateCodeFilter = '';
      const chart = Highcharts.mapChart('mapChart', {

        chart: {
            map: topology,
            borderWidth: 1
        },
  
        title: {
            text: `Annual net generation of power plants of the US in MWH and percentage of the plant's federal state.`
        },
  
        exporting: {
            sourceWidth: 600,
            sourceHeight: 500
        },
  
        legend: {
            layout: 'horizontal',
            borderWidth: 0,
            backgroundColor: 'rgba(255,255,255,0.85)',
            floating: true,
            verticalAlign: 'top',
            y: 25
        },
  
        mapNavigation: {
            enabled: true
        },
  
        colorAxis: {
            min: 1,
            type: 'logarithmic',
            minColor: '#EEEEFF',
            maxColor: '#000022',
            stops: [
                [0, '#EFEFFF'],
                [0.67, '#4444FF'],
                [1, '#000022']
            ]
        },
  
        series: [{
            animation: {
                duration: 1000
            },
            data: pointData,
            joinBy: ['postal-code', 'code'],
            dataLabels: {
                enabled: true,
                color: '#FFFFFF',
                format: '{point.code}'
            },
            name: 'State',
            tooltip: {
                pointFormat: '{point.code}'
            }
        },
        
       {
        type: 'mappoint',
          
        name: 'plant net value and percentage as per state',
        accessibility: {
          point: {
              valueDescriptionFormat: '{point.plantName}, {point.annualNetGenerationMWh}, {point.percentageGeneration}'
          }
      },

        color: 'red',
          tooltip: {
            pointFormat: '{point.plantName}, {point.annualNetGenerationMWh} {point.percentageGeneration}%'
        },
          data: toShow
      }
      ]
    }as any);         
    
     })
      })

    })

  }

  

}