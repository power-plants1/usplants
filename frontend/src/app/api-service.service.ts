import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(public http: HttpClient) { }
  serverUrl = `https://usplantsdamco.herokuapp.com`;
  // serverUrl = `http://localhost:80`;
  public getTopology() {
    return this.http.get('https://code.highcharts.com/mapdata/countries/us/us-all.topo.json');
  }
  public getData() {
    return this.http.get('https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/us-population-density.json');
  }
  public getPlantData(limit: any, filter: any){
    return this.http.get(`${this.serverUrl}/usplants/details/${limit}${filter}`,{headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }});
  }
}
